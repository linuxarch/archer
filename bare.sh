#!/bin/bash
# ------------------------------------------------------------------------
# archer - modular, semi automatic Arch Linux install script
# ------------------------------------------------------------------------

# INSTRUCTIONS -----------------------------------------------------------
#
# curl https://gitlab.com/linuxarch/archer/raw/master/bare.sh > install.sh
# bash install.sh

echo "Installing bare archlinux environment"

# RESPOSITORY ------------------------------------------------------------
REMOTE=https://gitlab.com/linuxarch/archer/raw/master

# CONFIG -----------------------------------------------------------------

# MODULES -----------------------------------------------------------------

# EXECUTE ----------------------------------------------------------------
. <(curl -fsL "${REMOTE}/modules/system/_functions.sh"); __LoadModule "system/_core"
