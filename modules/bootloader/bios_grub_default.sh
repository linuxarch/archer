# ------------------------------------------------------------------------
# BOOTLOADER
# ------------------------------------------------------------------------

__InstallPackage fuse os-prober grub

grub-install --target=i386-pc ${INSTALL_DRIVE}
grub-mkconfig -o ${SYSTEM_PARTITION}/grub/grub.cfg

# ------------------------------------------------------------------------
