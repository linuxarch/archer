# ------------------------------------------------------------------------
# FILESYSTEM
#

__QueryDrives;

BOOT_DRIVE=$INSTALL_DRIVE # expected format /dev/sda
PARTITION_BIOS=1
PARTITION_BOOT=2

PARTITION_ROOT=3
LABEL_BIOS=bios
LABEL_BOOT=boot

LABEL_ROOT=root
MOUNT_PATH=/mnt
SYSTEM_PARTITION=/boot

__FileSystem_Pre_BaseInstall () {
__PrintCountDown 5 "ERASING $INSTALL_DRIVE"

# disk prep
sgdisk -Z ${INSTALL_DRIVE}                           # zap all on disk
sgdisk -a 2048 -o ${INSTALL_DRIVE}                   # new gpt disk 2048 alignment

# create partitions
sgdisk -n ${PARTITION_BIOS}:0:+2M ${INSTALL_DRIVE}   # (BIOS), default start block, 2MB
sgdisk -n ${PARTITION_BOOT}:0:+256M ${INSTALL_DRIVE} # (BOOT), default start block, 256MB

sgdisk -n ${PARTITION_ROOT}:0:0 ${INSTALL_DRIVE}     # (ROOT), default start, remaining space

# set partition types
sgdisk -t ${PARTITION_BIOS}:EF02 ${INSTALL_DRIVE}
sgdisk -t ${PARTITION_BOOT}:8300 ${INSTALL_DRIVE}

sgdisk -t ${PARTITION_ROOT}:8300 ${INSTALL_DRIVE}

# label partitions
sgdisk -c ${PARTITION_BIOS}:"${LABEL_BIOS}" ${INSTALL_DRIVE}
sgdisk -c ${PARTITION_BOOT}:"${LABEL_BOOT}" ${INSTALL_DRIVE}

sgdisk -c ${PARTITION_ROOT}:"${LABEL_ROOT}" ${INSTALL_DRIVE}

# make filesystems
mkfs.ext4 ${INSTALL_DRIVE}${PARTITION_BOOT}
mkfs.ext4 ${INSTALL_DRIVE}${PARTITION_ROOT}



# mount target
mkdir -p ${MOUNT_PATH}
mount ${INSTALL_DRIVE}${PARTITION_ROOT} ${MOUNT_PATH}
mkdir -p ${MOUNT_PATH}${SYSTEM_PARTITION}
mount ${INSTALL_DRIVE}${PARTITION_BOOT} ${MOUNT_PATH}${SYSTEM_PARTITION}
}

__FileSystem_Post_BaseInstall () {
cat > ${MOUNT_PATH}/etc/fstab <<FSTAB_EOF
# /etc/fstab: static file system information
#
# <file system>					<dir>		<type>	<options>				<dump>	<pass>
tmpfs						/tmp		tmpfs	nodev,nosuid				0	0
/dev/disk/by-partlabel/${LABEL_BOOT}		$SYSTEM_PARTITION	ext4	rw,noatime,discard,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,errors=remount-ro	0 2
/dev/disk/by-partlabel/${LABEL_ROOT}				/      		ext4	rw,noatime,data=ordered,discard	0	1

FSTAB_EOF
}

__FileSystem_Pre_Chroot ()
{
umount ${MOUNT_PATH}${SYSTEM_PARTITION};
}

__FileSystem_Post_Chroot ()
{
mount ${INSTALL_DRIVE}${PARTITION_BOOT} ${SYSTEM_PARTITION} || return 1;
KERNEL_PARAMS="${KERNEL_PARAMS:+${KERNEL_PARAMS} }root=UUID=$(__GetDriveUuid ${INSTALL_DRIVE}${PARTITION_ROOT}) ro rootfstype=ext4"
}
