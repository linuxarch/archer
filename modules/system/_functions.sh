#!/bin/bash

__ValueSetDefault ()
{
# Assign value to a variable in the install script only if unset.
# Note that *empty* variables that have purposefully been set as empty
# are not changed.
#
# usage:
#
# __ValueSetDefault VARNAME "value if VARNAME is currently unset or empty"
#
eval "${1}=\"${!1-${2}}\"";
}

__ValueSet ()
{
# Assign a value to a "standard" bash format variable in a config file
# or script. For example, given a file with path "path/to/file.conf"
# with a variable defined like this:
#
# VARNAME=valuehere
#
# the value can be changed using this function:
#
# __ValueSet newvalue VARNAME "path/to/file.conf"
#
valuename="$1" newvalue="$2" filepath="$3";
sed -i "s+^#\?\(${valuename}\)=.*$+\1=${newvalue}+" "${filepath}";
}

__ValueDisable ()
{
# Comment out a value in "standard" bash format. For example, given a
# file with a variable defined like this:
#
# VARNAME=valuehere
#
# the value can be commented out to look like this:
#
# #VARNAME=valuehere
#
# using this function:
#
# __ValueDisable VARNAME "path/to/file.conf"
#
valuename="$1" filepath="$2";
sed -i "s/^\(${valuename}.*\)$/#\1/" "${filepath}";
}

# UNCOMMENTVALUE ---------------------------------------------------------
__ValueEnable ()
{
# Uncomment out a value in "standard" bash format. For example, given a
# file with a commented out variable defined like this:
#
# #VARNAME=valuehere
#
# the value can be UNcommented out to look like this:
#
# VARNAME=valuehere
#
# using this function:
#
# __ValueEnable VARNAME "path/to/file.conf"
#
valuename="$1" filepath="$2";
sed -i "s/^#\(${valuename}.*\)$/\1/" "${filepath}";
}

# ADDTOLIST --------------------------------------------------------------
__VariableAddToList ()
{
# Add to an existing list format variable (simple space delimited list)
# such as VARNAME="item1 item2 item3".
#
# Handles lists enclosed by either "quotes" or (parentheses)
#
# Usage (internal variable)
# __VariableAddToList "new item" newitem newitem
#
if [ "$#" -lt 3 ]; then
newitem="$1" listname="$2"
eval "${listname}=\"${!listname} $newitem\""
else # add to list variable in an existing file
newitem="$1" listname="$2" filepath="$3";
sed -i "s_\(${listname}\s*=\s*[^)]*\))_\1 ${newitem})_" "${filepath}";
sed -i "s_\(${listname}\s*=\s*\"[^\"]*\)\"_\1 ${newitem}\"_" "${filepath}";
fi
}

__PromptPressAnyKey ()
{
# Provide an alert (with optional custom preliminary message) and pause.
#
# Usage:
# __PromptPressAnyKey "optional custom message"
#
echo -e "\n$@"; read -sn 1 -p "Any key to continue..."; echo;
}

# DOUBLE CHECK UNTIL MATCH -----------------------------------------------
__CheckTwiceUntilMatch ()
{
# ask for input twice for match confirmation; loop until matches
entry1="x" entry2="y"
while [ "$entry1" != "$entry2" -o -z "$entry1" ]; do
read -s -p "${1:-Passphrase}: " entry1
echo
read -s -p "${1:-Passphrase} again: " entry2
echo
if [ "$entry1" != "$entry2" ]; then
    echo -e "\n${1:-Passphrase} entry doesn't match.\n" 
elif [ -z "$entry1" ]; then
    echo -e "\n${1:-Passphrase} cannot be blank.\n" 
fi
done
_DOUBLE_CHECK_RESULT="$entry1"
}

__TryUntilSuccess ()
{
# first argument is statement that must evaluate as true in order to continue
# optional second argument specifies number of tries to limit to
_tries=0; _failed=true; while $_failed; do
_tries=$((_tries+1))
eval "$1"
[ $? -eq 0 ] && _failed=false;
[ -n "$2" -a $_tries -gt $2 ] && return 1;
done
return 0
}

__InstallPackage ()
{
# Install package(s) from official repositories, no confirmation needed.
# Takes single or multiple package names as arguments.
#
# Usage:
# __InstallPackage pkgname1 [pkgname2] [pkgname3]
#
pacman -S --noconfirm "$@";
}

__InstallPackageAUR ()
{
# Install package(s) from arch user repository, no confirmation needed.
# Takes single or multiple package names as arguments.
#
# Installs default helper first ($AURHELPER)
#
# Usage:
# __InstallPackageAUR pkgname1 [pkgname2] [pkgname3]
#
__ValueSetDefault AURHELPER packer
if command -v $AURHELPER >/dev/null 2>&1; then
    $AURHELPER -S --noconfirm "$@";
else
    pkg=$AURHELPER;
    orig="$(pwd)";
    build_dir=/tmp/build/${pkg};
    mkdir -p $build_dir;
    chgrp nobody $build_dir;
    chown -R nobody $build_dir;
    chmod -R 777 $build_dir;
    
    cd $build_dir;
    for req in wget git expac jshon; do
        command -v $req >/dev/null 2>&1 || __InstallPackage $req;
    done
    wget "https://aur.archlinux.org/cgit/aur.git/snapshot/${pkg}.tar.gz";
    tar -xzvf ${pkg}.tar.gz;
    chown -R nobody $build_dir;
    chmod -R 777 $build_dir;
    chmod g+s $build_dir;
    setfacl -d -m g::rwx $build_dir;
    setfacl -d -m o::rx $build_dir;
    setfacl -d — set u::rwx,g::rwx,o::- $build_dir;
    cd ${pkg};
    sudo -u nobody makepkg -si --noconfirm; cd "$orig"; rm -rf $build_dir;
    $AURHELPER -S --noconfirm "$@";
fi;
}

__ChrootPostscript ()
{
# handle interactively assigned install drive value
echo -e "#!/bin/bash\nINSTALL_DRIVE=$INSTALL_DRIVE" > "${MNT}${POSTSCRIPT}";
grep -v "^\s*INSTALL_DRIVE.*" "${0}" >> "${MNT}${POSTSCRIPT}";
#cp "${0}" "${MNT}${POSTSCRIPT}";
chmod a+x "${MNT}${POSTSCRIPT}"; arch-chroot "${MNT}" "${POSTSCRIPT}";
}

__PrintCountDown ()
{
# countdown 10 "message here"
#
for i in `seq $1 -1 1`; do
echo -en "\r$2 in $i seconds (ctrl-c to cancel and exit) <"
for j in `seq 1 $i`; do echo -n "=="; done; echo -en "     \b\b\b\b\b"
sleep 1; done; echo
}


__LoadModule ()
{
[ -z "$@" ] && return
for _module in $@; do
isurl=false ispath=false isrootpath=false;
case "$_module" in
    *://*) isurl=true ;;
    /*)    isrootpath=true ;;
    */*)   ispath=true ;;
esac
FILE="${_module/%.sh/}.sh";
if $isurl; then URL="${FILE}";
elif [ -f "${DIR/%\//}/${FILE}" ]; then URL="file://${FILE}";
else URL="${REMOTE/%\//}/modules/${FILE}"; fi

echo -e "FILE = ${FILE}"
echo -e "URL  = ${URL}"
_loaded_module="$(curl -fsL ${URL})";

#set +e
[ -n "$_loaded_module" ] && eval "${_loaded_module}";
if [ "$?" -gt 0 ]; then
__PromptPressAnyKey "Execution of module \"$_module\" experienced errors"
fi
#set -e
done
} 

__PromptConfirm ()
{
# prompt user for input, confirm that input is correct before proceeding
# outputs to global QUERYRESPONSE variable
shopt -s nocasematch
_confirmation=x
while [ "$_confirmation" != "y" ]; do
echo
read -p "${1:-Enter value}: " QUERYRESPONSE
read -sn 1 -p "Is this correct: $QUERYRESPONSE (y/n) " _confirmation
done
echo
shopt -u nocasematch
}

__QueryDrives ()
{
# displays list of drives for initial selection
# sets $DRIVE to queried install drive selection
#
echo -e "\nInstall drive selection\n-----------------------\n"
lsblk -d | grep -v "loop"
if [ -z "$INSTALL_DRIVE" -o "$INSTALL_DRIVE" == "query" -o "$INSTALL_DRIVE" == "QUERY" ]; then
    unset INSTALL_DRIVE
    echo -e "\nPlease enter full drive path starting with /dev (e.g. /dev/sda) that you wish to use as the install drive. NOTE THAT THIS INSTALL DRIVE WILL BE ERASED.\n"
else
    echo -e "\nConfig value already set to $INSTALL_DRIVE.\n"
fi
_invalid_entry=true; while $_invalid_entry; do
[ -z "$INSTALL_DRIVE" ] && read -p "INSTALL DRIVE: " INSTALL_DRIVE
if [ -z "$INSTALL_DRIVE" ]; then echo -e "\nNo value entered.\n"; unset INSTALL_DRIVE
elif ! echo "$INSTALL_DRIVE" | grep -q "^/dev.*"; then echo -e "\nPlease prefix entry with /dev.\n"; unset INSTALL_DRIVE
elif echo "$INSTALL_DRIVE" | grep -q ".*[0-9]$"; then echo -e "\nPlease enter a root volume, not a partition number (e.g. /dev/sda, not /dev/sda1).\n"; unset INSTALL_DRIVE
else _invalid_entry=false; fi;
done;
}

__LoadEfiModules ()
{
# Load efivars (or confirm they've loaded already) and set EFI_MODE for
# later use by bootloader.
#
modprobe efivars || true;
ls -l /sys/firmware/efi/vars/ &>/dev/null && return 0 || return 1;
}

__GetDriveUuid ()
{
# usage:
# __GetDriveUuid /dev/sda3
MATCH="$(echo "$1" | sed "s_/_\\\/_g")"
blkid -c /dev/null | sed -n "/${MATCH}/ s_.*UUID=\"\([^\"]*\).*_\1_p"
}

# ENABLE REPOSITORIES FOR SPECIFIC LANGUAGES/FRAMEWORKS ------------------
_enable_haskell_repos ()
{

# add repos to /etc/pacman.conf

#egrep -q "^\[haskell-testing\]" /etc/pacman.conf || \
#sed -i '/^\[core\]/i \
#[haskell-testing]\
#Server = http://www.kiwilight.com/haskell/testing/$arch\
#' /etc/pacman.conf

egrep -q "^\[haskell-extra\]" /etc/pacman.conf || \
sed -i '/^\[core\]/i \
[haskell-extra]\
Server = http://archhaskell.mynerdside.com/$repo/$arch\
' /etc/pacman.conf

egrep -q "^\[haskell\]" /etc/pacman.conf || \
sed -i '/^\[core\]/i \
[haskell]\
Server = http://xsounds.org/~haskell/$arch\
' /etc/pacman.conf

# update new repos

pacman --noconfirm -Sy

}

# MISC FUNCTIONS ---------------------------------------------------------

__SetFont () { setfont $FONT; }

# NULL FUNCTIONS (OVERRIDDEN BY EPONYMOUS BLOCKS)-------------------------
__FileSystem_Pre_BaseInstall () { :; }
__FileSystem_Post_BaseInstall () { :; }
__FileSystem_Pre_Chroot () { :; }
__FileSystem_Post_Chroot () { :; }
