#!/bin/bash

# PREFLIGHT --------------------------------------------------------------

# buckle up
#set -o errexit

# check if we're in an IO redirect or incorrectly sourced script
[ ! -f "${0}" ] && echo "Don't run this directly from curl. Save to file first." && exit

# set mount point, temp directory, script values
MNT=/mnt
TMP=/tmp/archermodules
POSTSCRIPT="/post-chroot.sh"

# get chroot status
[ -e "${POSTSCRIPT}" ] && INSIDE_CHROOT=true || INSIDE_CHROOT=false

# DEFAULT REPOSITORY URL -------------------------------------------------
# (probably not useful here if initialization script has already used it,
# but retained here for reference)

__ValueSetDefault REMOTE https://gitlab.com/linuxarch/archer/tree/master

# DEFAULT CONFIG VALUES --------------------------------------------------

__ValueSetDefault HOSTNAME archer
__ValueSetDefault USERNAME deccer
__ValueSetDefault USERSHELL /bin/bash
__ValueSetDefault FONT Lat2-Terminus16
__ValueSetDefault FONT_MAP 8859-1_to_uni
__ValueSetDefault LANGUAGE en_US.UTF-8
__ValueSetDefault KEYMAP us
__ValueSetDefault TIMEZONE Europe/Berlin
__ValueSetDefault MODULES ""
__ValueSetDefault HOOKS "base udev autodetect modconf block filesystems fsck"
__ValueSetDefault KERNEL_PARAMS # "quiet" # set/used in FILESYSTEM,INIT,BOOTLOADER modules
__ValueSetDefault AURHELPER packer
__ValueSetDefault INSTALL_DRIVE query # this overrides any default value set in FILESYSTEM module

#TODO: REMOVE THIS #_defaultvalue PRIMARY_BOOTLOADER UEFI # UEFI or BIOS (case insensitive)

# CONFIG VALUES WHICH REMAIN UNDEFAULTED ---------------------------------
# for reference - these remain unset if not already declared
# USERNAME, SYSTEMTYPE

# MODULES DEFAULTS --------------------------------------------------------

__ValueSetDefault FILESYSTEM filesystem/gpt_single_noswap_ext4
__ValueSetDefault INSTALL system/install_pacstrap
__ValueSetDefault HARDWARE ""
__ValueSetDefault TIME system/setup_time_utc
__ValueSetDefault SETLOCALE system/setup_locale
__ValueSetDefault HOST system/setup_hostname
__ValueSetDefault RAMDISK system/compile_ramdisk_environment
__ValueSetDefault BOOTLOADER bootloader/bios_grub_default
__ValueSetDefault NETWORK network/wired_dhcp_default
__ValueSetDefault ROOTUSER "system/setup_rootpassword system/setup_sudouser"
__ValueSetDefault XORG ""
__ValueSetDefault AUDIO ""
__ValueSetDefault VIDEO ""
__ValueSetDefault SOUND ""
__ValueSetDefault POWER ""
__ValueSetDefault SENSORS ""
__ValueSetDefault DESKTOP ""
__ValueSetDefault APPLICATIONS ""
__ValueSetDefault PACKAGES "git"
__ValueSetDefault PACKAGES_FROM_AUR "git"

# Archlinux Install / Pre Chroot
if ! $INSIDE_CHROOT; then
__SetFont                         # SET FONT FOR PLEASANT INSTALL EXPERIENCE
__LoadEfiModules || true          # ATTEMPT TO LOAD EFIVARS, EVEN IF NOT USING EFI (REQUIRED)
__LoadModule "${FILESYSTEM}"      # LOAD FILESYSTEM (FUNCTIONS AND VARIABLE DECLARATION ONLY)
__FileSystem_Pre_BaseInstall      # FILESYSTEM CREATION AND CONFIG
__LoadModule "${INSTALL}"         # INSTALL ARCH
__FileSystem_Post_BaseInstall     # WRITE FSTAB/CRYPTTAB AND ANY OTHER POST INTALL FILESYSTEM CONFIG
__FileSystem_Pre_Chroot           # PROBABLY UNMOUNT OF BOOT IF INSTALLING UEFI MODE
__ChrootPostscript                # CHROOT AND CONTINUE EXECUTION
fi

# Arch Configuration / Post Chroot
if $INSIDE_CHROOT; then
umount /tmp || __PromptPressAnyKey "/tmp isn't unmounted..."
__LoadModule "${FILESYSTEM}"      # LOAD FILESYSTEM FUNCTIONS
pacman -Sy
__FileSystem_Post_Chroot         # FILESYSTEM POST-CHROOT CONFIGURATION
__LoadModule "${SETLOCALE}"       # SET LOCALE
__LoadModule "${TIME}"            # TIME
__LoadModule "${HOST}"            # HOSTNAME
__LoadModule "${NETWORK}"         # NETWORKING
__LoadModule "${AUDIO}"           # AUDIO
__LoadModule "${VIDEO}"           # VIDEO
__LoadModule "${SOUND}"           # SOUND
__LoadModule "${POWER}"           # POWER
__LoadModule "${SENSORS}"         # SENSORS
#__LoadModule "${KERNEL}"         # KERNEL
__LoadModule "${RAMDISK}"         # RAMDISK
__LoadModule "${BOOTLOADER}"      # BOOTLOADER
__LoadModule "${ROOTUSER}"        # COMMON POST INSTALL ROUTINES
__LoadModule "${XORG}"            # XORG
__LoadModule "${DESKTOP}"         # DESKTOP/WM/ETC
__LoadModule "${HARDWARE}"        # COMMON POST INSTALL ROUTINES
__LoadModule "${APPLICATIONS}"         # COMMON APPLICATION/UTILITY SETS
__InstallPackage ${PACKAGES}
__InstallPackageAUR ${PACKAGES_FROM_AUR}
fi

