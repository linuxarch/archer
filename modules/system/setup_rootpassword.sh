#!/bin/bash

# root password
echo -e "${HR}\\nNew root user password\\n${HR}"
__TryUntilSuccess "passwd" 5 || echo -e "\nERROR: password not set for root\n"
